" This .vimrc doesn't have any particular structure.
" I've collected settings that make the most sense to me,
" so it is possible these are not of any use to you.
" Use at your own discretion.

:set nocompatible


" load .bashrc (or .bash_profile)
:set shellcmdflag=-ic

" set tab as 2 spaces, indentation with 2 spaces
:set tabstop=2 shiftwidth=2 expandtab

:set enc=utf8
:set fileencoding=utf8

" auto-indent
:set ai

" syntax hilight
:syntax on

" ignore case when searching...
:set ignorecase

" ...unless the search term contains an uppercase letter
:set smartcase

" relative line numbering
:set relativenumber
" absolute line numbering on the current line
:set number

" hilight search matches
:set hlsearch
" jump automatically to the first match of the search
:set incsearch

:set backspace=eol,start,indent

" wrap to next/previous line when navigating with arrow keys or h, l
:set whichwrap+=<,>,h,l

" theme
:colorscheme jellybeans

" display status bar
:set laststatus=2

" compile LaTeX file three times (M.Sc. thesis related, probably not useful in
" your case
:map <F10> :w<CR>:! ./g2pdf.sh && ./g2pdf.sh && ./g2pdf.sh<CR><CR>

" always display at least 10 lines above and below the cursor
:set scrolloff=10

" insert an empty line below the current line
:nnoremap <silent> <c-j> :silent call append('.', '')<cr>
" insert an empty line above the current line
:nnoremap <silent> <c-k> :silent call append(line('.')-1, '')<cr>

" open vsplits to right, hsplits to below
:set splitright
:set splitbelow

" preserve folds
:autocmd BufWinLeave *.* mkview
:autocmd BufWinEnter *.* silent loadview

" absolute line numbers on focus
" :au FocusLost * :set number

" relative line numbers when focus lost
" :au FocusGained * :set rnu

" M.Sc. thesis related, probably not useful in your case
:ab ResnickEtAl ResnickAndMaloneyAndMonroy-HernandezAndRuskAndEastmondAndBrennanAndMillnerAndRosenbaumAndSilverAndSilvermanAndKafai2009
:ab MaloneyEtAl MaloneyAndResnickAndRuskAndSilvermanAndEastmond2010
:ab ElliottEtAl ElliottAndVijayakumarAndZinkAndHansen2007
:ab McKeithenEtAl McKeithenAndReitmanAndRueterAndHirtle1981
:ab msaba2013 Meerbaum-SalantAndArmoniAndBen-Ari2013
:ab vhp2017 Velazquez-IturbideAndHernan-LosadaAndParedes-Velasco2017
:ab lsrhs2009 LonsdaleAndSabistonAndRaedekeAndHaAndSum2009
